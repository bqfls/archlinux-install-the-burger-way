# PRE-INSTALL STUFF TO DO

**Disabling Secure Boot**

Secure Boot can be used on Arch Linux but the [installation medium doesn't support it](https://wiki.archlinux.org/title/Installation_guide#Boot_the_live_environment). You have to enter your firmware settings and find your Secure Boot settings and turn Secure Boot off.