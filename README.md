# archlinux-install-the-burger-way
Installing Arch Linux, the way I do it.

# WORK IN PROGRESS : ALL THE STEPS MAY NOT BE COVERED IN DETAIL AND/OR MAY NOT BE COVERED AT ALL!!!

# STEP 0 : PARTITION THE DISK
Pretty self explanatory. If you have a completely empty drive, it shows you how to give it a partitioning table (GPT is the modern partitioning table, and what I use too.), how to make two basic partitions (one for a EFI partition assuming your system does not utilise the older BIOS, and one for the root directory.) etc.

# STEP 1 : MAKE THE FILESYSTEMS
If you do not have a completely empty drive and, say, know what partitions you are exactly going to install Arch Linux onto, the guide covers it as well. If your system already has partitions but you do not know what to substitute for, then you can always do `lsblk -f` from the live Arch installer session. This is for a system that already has a EFI partition (ex. any laptop preinstalled with Windows.), and has a known partition dedicated for a root directory.

>Note : This guide does not show you how to make a separate home directory. While useful, I personally don't really care about that as much.

# STEP 2 : SET UP ENCRYPTED DRIVES AND MOUNT THEM
Make your drives encrypted. I use the ext4 filesystem, but you are free to use btrfs if you like it. Use `mkfs.btrfs /dev/mapper/ROOT` instead of `mkfs.ext4` if you want btrfs.

# STEP 3 : INSTALL ESSENTIAL PACKAGES AND GENERATE FILESYSTEM TABLE
Installs the absolutely essential base package, along with the most recent Linux kernel release available from the Arch Linux repositories. Install the microcode from Intel or AMD too. You can choose to ignore this if you are concerned about free software-ness, but your experience may be worsened by your CPU being borked.

>Note 1 : You can choose to install other Linux kernels such as linux-lts or linux-hardened. However, I use the vanilla Linux kernel only, so my knowledge on those kernels and debugging support will be limited.

>Note 2 : nano is the tool I use for this, but other command line text editors such as vim can work, provided you know copy and paste (which is essential for STEP 5).

# STEP 4 : DOING STUFF IN THE NEWLY INSTALLED SYSTEM

You've just installed your Arch system. Hooray! Except, it isn't ready for prime-time yet. This section allows you to setup basic things like changing the hostname and generating locales. Also generates initamfs and what not. 

> Note : For the `mkinitcpio -p linux` command, you should swap "linux" with the name of the kernel you installed (for example, linux-hardened) to generate the correct initramfs.

# STEP 5 : INSTALL BOOTLOADER AND CONFIGURE TO DECRYPT DRIVES UPON BOOT

Installs the bootloader. Since we use encrypted drives for this tutorial, we have to configure the bootloader to detect the correct drives and allow us to decrypt them.

>Note : GRUB is what I used for this tutorial. The way to edit the config is a bit messy but it should get the job done provided you clean up the unnecessary output. 

**SIDENOTE GUIDENOTE**
GRUB config is probably the most annoying thing about this. For an ez fix, just follow these steps. (Substitute "grubconfig.txt" with the actual filepath, this is just a dummy file to show you how to do the config.)

1. First, throw the lsblk output into the grub config file (this makes it easier to copy stuff).

https://git.envs.net/bqfls/archlinux-install-the-burger-way/raw/commit/3b9b54f64a9a8b2ebe054a3db77f4af49efc3632/images/step1.png

2. Then, use nano to open the text file.

https://git.envs.net/bqfls/archlinux-install-the-burger-way/raw/commit/3b9b54f64a9a8b2ebe054a3db77f4af49efc3632/images/step2.png

3. Then, using the down arrow key, go to the bottom until you see the lsblk output.

https://git.envs.net/bqfls/archlinux-install-the-burger-way/raw/commit/3b9b54f64a9a8b2ebe054a3db77f4af49efc3632/images/step3.png

4. Use the arrow keys to move the cursor to the the beginning letter of the UUID of your root partition (something like /dev/sdX under which you should see "cryptdevice"). Then, while holding Shift, move the right arrow key to the **RIGHT** of the last letter/number of the UUID. The entire UUID should be selected. (see image below)

https://git.envs.net/bqfls/archlinux-install-the-burger-way/raw/commit/531303a0edf13abce795e5cbe419b7233c57d59c/images/step4.png

5. Then, use Ctrl + K to cut the UUID. Scroll up with the arrow keys until you reach GRUB_CMDLINE_LINUX_DEFAULT. Then, move the cursor onto the quote.

https://git.envs.net/bqfls/archlinux-install-the-burger-way/raw/commit/531303a0edf13abce795e5cbe419b7233c57d59c/images/step5.png

6. Press space to leave a space between any other flags. Type "cryptdevice=UUID=" without quotes. Then, press Ctrl + U to paste the UUID of the partition /dev/sdX (should look like image below).

https://git.envs.net/bqfls/archlinux-install-the-burger-way/raw/commit/3b9b54f64a9a8b2ebe054a3db77f4af49efc3632/images/step6.png

7. Go down to the lsblk output until you see "cryptdevice". Using the methods froms step 4, copy the UUID of "cryptdevice".

https://git.envs.net/bqfls/archlinux-install-the-burger-way/raw/commit/3b9b54f64a9a8b2ebe054a3db77f4af49efc3632/images/step7.png

8. Go back up to the same place where we put the UUID of the root partition (i.e GRUB_CMDLINE_LINUX_DEFAULT) and go to the end quotes again. Then, at the very end, type ":cryptdevice root=UUID=" (without quotes). Then, paste the UUID of the cryptdevice. Your whole GRUB_CMDLINE_LINUX_DEFAULT config should look like the second image attached below.

https://git.envs.net/bqfls/archlinux-install-the-burger-way/raw/commit/3b9b54f64a9a8b2ebe054a3db77f4af49efc3632/images/step8.png

https://git.envs.net/bqfls/archlinux-install-the-burger-way/raw/commit/3b9b54f64a9a8b2ebe054a3db77f4af49efc3632/images/step8dot1.png

9. Almost done! Now, go to below again and select all of the extra lsblk output. Then, using Ctrl + K should cut out the extra stuff. You can then do a Ctrl + X and save the file. You can now decrypt upon boot. Hooray!

https://git.envs.net/bqfls/archlinux-install-the-burger-way/raw/commit/3b9b54f64a9a8b2ebe054a3db77f4af49efc3632/images/step9.png

# STEP 6 : SYSTEM CONFIGURATION

Login as the root user and create your own, regular-privileged user. Sets up stuff like DHCP, zram (basically swapspace for your computer), ACPI (power management stuff), CUPS (for printing), etc.

> Note : It is possible to user swap partition instead of zram. You *can* do the following, granted zram is usually preferred to this method. It can be also done after installation.

>Create a partition, let's say, /dev/sdaZ. It can be of any size, preferably half the total amount of RAM.
Now, in a live boot session in the Arch iso, enter the following

>mkswap /dev/sdaZ
>swapon /dev/sdaZ
>genfstab -U /mnt > /etc/fstab
