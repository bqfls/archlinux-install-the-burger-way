Packages that are not hard dependencies of software but adds a lot of required functionality :

ffmpegthumbs (video thumbnails)
kdegraphics-thumbnailers (image thumbnails)
kimageformats (some image formats like HEIC don't load without this)
adobe-source-han-sans-jp-fonts (japanese sans fonts)
adobe-source-han-sans-kr-fonts (korean sans fonts)
adobe-source-han-serif-jp-fonts (japanese serif fonts)
adobe-source-han-serif-kr-fonts (korean serif fonts)
bash-completion (bash completion, duh)
bind (some dns stuff)
firewalld [included in guide]
dhcpcd [included in guide]
gutenprint (printing stuff for CUPS)
gnu-free-fonts (some fonts such as Times New Roman (FreeSerif))
kdeconnect (phone connecting)
kio-admin (dolphin admin access)
kolourpaint (ms paint for linux)
ktorrent (iso torrenting)
kvantum (application styles theming)
libreoffice-fresh (office suite)
man-db (man)
man-pages (man)
os-prober (too add entry to grub, if dual booting)
power-profiles-daemon (power saver, balanced and perf modes for battery stuff)
sane (scanner)
sane-airscan (wireless scanning)
skanpage (kde graphical scanning util)
spectacle (kde screenshot util)
switcheroo-control (dual gpu management)
system-config-printer (printer stuff)
vim (text editor)
xdg-desktop-portal-gtk (to fix broken gtk apps on kde)
xdg-desktop-portal-wlr (screenshare on wayland)
zram-generator (for zram)

